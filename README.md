# otrie

An octet trie for quickly matching IPv4 addresses.

## Example

```go
prefixesStr := `10.255.251.48/30
10.255.251.52/30
10.51.4.0/24
10.52.4.0/24`

trie := otrie.NewOTrie()
prefixes := make([]*netaddr.NetAddr, 0, 128)
for _, l := range strings.Split(prefixesStr, "\n") {
	na, _ := netaddr.IP(l)
	prefixes = append(prefixes, na)
	_ = trie.AddPrefix(na)
}
if !trie.Overlaps(&netaddr.NetAddr{
	Address: []byte{10, 51, 4, 0},
	Length:  byte(24),
}) {
	fmt.Println("failed to match 10.51.4.0/24")
	fmt.Println(trie.Print())
}
```

