module idio.link/go/otrie

go 1.16

retract (
	v0.0.6
	v0.0.5
	v0.0.4
	v0.0.3
	v0.0.2
	v0.0.1
)

require idio.link/go/netaddr/v2 v2.1.0
